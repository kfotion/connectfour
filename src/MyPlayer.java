import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 */
public class MyPlayer extends Player {

    Random rand;
    int turnNumber;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        turnNumber = 0;
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }

    /**
     * Selects move with best likelhood of a helpful outcome
     * with center column hardcoded in as best first move
     * @param gameBoard
     * @return bestMove
     */
    public int chooseMove(Board gameBoard) {

        Move bestMove = null;
        long start = System.nanoTime();

        //If the middle column is empty, choose that move
        if(turnNumber == 0) {
            bestMove = new Move(3);
        }
        else {
            bestMove = search(gameBoard, 6, this.playerNumber);
        }

        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        turnNumber++;

        return bestMove.move;
    }

    /**
     * Searches to give each possible move a "helpfulness" value
     * First looks six moves ahead, then applies an additional heuristic to estimate outcome if necessary
     * @param gameBoard
     * @param maxDepth
     * @param playerNumber
     * @return best move from all possible moves
     */
    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i=0; i<Board.BOARD_SIZE; i++) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }

        // Pick the highest value move
        return this.getBestMove(moves);

    }

    /**
     * Estimates the value of a board configuration
     * Returns close to 1.0 if in favor of playerNumber, returns close to -1.0 if in favor of other player (on a scale)
     * @param gameBoard
     * @param playerNumber
     * @return value of board configuration
     */
    public double heuristic(Board gameBoard, int playerNumber) {

        //Construct 3D integer array to replicate gameboard
        int[][] gameBoardArr = gameBoard.getBoard();

        //Calculate the number of 2s and 3s in each row, +1 means one 2 in a row, +2 means one 3 in a row or two 2s
        int consecutivesInRow = 0;
        int[] row = new int[Board.BOARD_SIZE];
        for(int y = 0; y < Board.BOARD_SIZE; y++) {
            for (int x = 0; x < Board.BOARD_SIZE; x++) {
                row[x] = gameBoardArr[x][y];
            }
            consecutivesInRow = consecutivesInRow + countConsecutives(playerNumber, row, 0, 0);
        }

        //Calculate the number of 2s and 3s in each column
        int[] column = new int[Board.BOARD_SIZE];
        for (int x = 0; x < Board.BOARD_SIZE; x++) {
            for (int y = 0; y < Board.BOARD_SIZE; y++) {
                column[y] = gameBoardArr[x][y];
            }
            consecutivesInRow = consecutivesInRow + countConsecutives(playerNumber, column, 0, 0);
        }

        //Calculate the number of 2s and 3s in each right diagonal
        for (int z = -Board.BOARD_SIZE + 1; z < Board.BOARD_SIZE; z++) {
            int[] diagonalR = new int[Board.BOARD_SIZE - Math.abs(z)];
            for (int x = 0; x < Board.BOARD_SIZE - Math.abs(z); x++) {
                if (z < 0)
                    diagonalR[x] = gameBoardArr[x][-z + x];
                else if (z > 0)
                    diagonalR[x] = gameBoardArr[z + x][x];
                else
                    diagonalR[x] = gameBoardArr[x][x];
            }
            consecutivesInRow = consecutivesInRow + countConsecutives(playerNumber, diagonalR, 0, 0);
        }

        //Calculate the number of 2s and 3s in each left diagonal
        for (int z = -Board.BOARD_SIZE + 1; z < Board.BOARD_SIZE; z++) {
            int[] diagonalL = new int[Board.BOARD_SIZE - Math.abs(z)];
            for (int x = 0; x < Board.BOARD_SIZE - Math.abs(z); x++) {
                if (z < 0)
                    diagonalL[x] = gameBoardArr[x][Board.BOARD_SIZE - 1 + z - x];
                else if (z > 0)
                    diagonalL[x] = gameBoardArr[z + x][Board.BOARD_SIZE - 2 - x];
                else
                    diagonalL[x] = gameBoardArr[x][Board.BOARD_SIZE - 1 - x];
            }
        }

        double num = (double)(consecutivesInRow);
        double denom = (double)(turnNumber+2);
        double result = (double)(num/denom);

        return result;
    }

    /**
     * Private helper method to count the number of consecutive numbers in a 2D array
     * add if playerNumber has consecutives, subtract if other player has consecutives
     * add 1 if 2 in a row, add 2 if 3 in a row
     * @param playerNumber
     * @param arr
     * @param index
     * @param numConsecutives
     * @return numConsecutives
     */
    private int countConsecutives(int playerNumber, int[] arr, int index, int numConsecutives) {

        //Iterate through 2D array and keep track of which player has consecutives
        boolean startCounting = false;
        int lastSpace = 0;
        for (int i = index; i < arr.length; i++) {
            int space = arr[i];
            if (space == 0 && lastSpace != 0)
                continue;
            if(startCounting && (space == lastSpace || lastSpace == 0)) {
                numConsecutives++;
            }
            else if (!startCounting && space != 0 && space == lastSpace) {
                numConsecutives--;
            }
            if(space == playerNumber) {
                startCounting = true;
            }
            else
                startCounting = false;
            lastSpace = space;
        }

        //Return value measuring consecutives
        return numConsecutives;

    }

    /**
     * Private helper method to select best move from ArrayList
     * @param moves
     * @return bestMove
     */
    private Move getBestMove(ArrayList<Move> moves) {

        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }

}
